---
layout: job_family_page
title: "Executive Assistant"
---

## Responsibilities

* Executive assistants at GitLab toggle seamlessly between various systems including G-Suite, Slack, Expensify, Zoom and GitLab to accomplish an array of tasks, while staying focused on prioritization and escalating urgent issues
* We’re searching for a self-driven, collaborative, and agile team member who is experienced in managing multiple priorities, juggling various responsibilities, and anticipating
executives’ needs
* The ideal candidate will be exceptionally organized personally and enjoys organizing for others, has a deep love of logistics and thrives in a dynamic start-up environment.
* Support our Executive Leadership Team in EST - PST timezones
* Manage complex calendar including vetting, prioritizing and providing recommendations
* Total travel coordination including air and ground transportation, hotel reservations, security, visas and other travel documentation
* Maintain an efficient flow of information between all levels including internal and external contacts on a wide spectrum of plans and priorities
* Draft internal and external communications on behalf of executive
* File expense reports and track reimbursement status
* Manage projects, internal and external meetings and large-scale events from budget planning through logistical coordination
* May coordinate, track and schedule internal and external PR and communication events for manager and team
* Partner with PeopleOps, IT and Security to resolve any logistical issues
* Assist the full life cycle of recruiting including booking interviews, liaising between the hiring team and the candidate, and coordinating onboarding of new hires
* Maintains and tracks manager and teams project list and goals
* Attend team staff meetings, track agenda and assist Executive to keep team on course
* Provide general project support as needed including ad-hoc reporting
* Provide coverage for other E-Group EAs
* Oversee the recruiting and interviewing of team EA candidates
* Provide direction and coaching to other EA staff and establish best practices
* We're looking for a talented communicator and an expert planner to ensure GitLab continues to run smoothly.
* Other duties as assigned in support of the business


## Requirements
* Minimum of 3 years of executive administration supporting more than one executive
* Must be able to support team members in PST and match the CEO's working schedule
* Successful history of managing the calendars, expenses, and travel of multiple executives
* Experience predicting, prioritizing, and assisting an executive’s workload
* Extensive technical skills in Google Suite, Zoom, Slack, and Expensify
* Must be proactive and able to deal with ambiguity, prioritize own work and resources, and juggle multiple tasks in a manner transparent to the team, and work independently to achieve results with a high degree of accuracy
* Exceptional communication and interpersonal skills and ability to interact autonomously with internal and external partners
* Maintain the confidentiality of highly sensitive material with tact and professionalism
* Excellent computer skills, self starter in picking up new and complex systems
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor)
* Possess an ability to multi-task and prioritize in a dynamic environment
* Superior attention to detail
* Event coordination and creative event planning experience
* Excellent written and verbal English communication skills
* Experience in a start-up environment preferred
* Experience working remotely preferred
* A desire to learn, have fun an work hard
* A passion for GitLab
* A sincere willingness to help out
* Able to work collaboratively with Administrative teammates across the org
* An orientation towards team success
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
